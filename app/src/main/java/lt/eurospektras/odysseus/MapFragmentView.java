/*
 * Copyright (c) 2011-2020 HERE Europe B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lt.eurospektras.odysseus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.guidance.TruckRestrictionsChecker;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.RouteElement;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;
import com.here.android.mpa.routing.TruckRestriction;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This class encapsulates the properties and functionality of the Map view.A route calculation from
 * HERE Burnaby office to Langley BC is also being handled in this class
 */
public class MapFragmentView {
    private AndroidXMapFragment m_mapFragment;
    private AppCompatActivity m_activity;
    private Map m_map;
    private MapRoute m_mapRoute;

    public MapFragmentView(AppCompatActivity activity) {
        m_activity = activity;
        initMapFragment();
        /*
         * We use a button in this example to control the route calculation
         */
        initCreateRouteButton();
    }

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) m_activity.getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }


    private void initMapFragment() {
        /* Locate the mapFragment UI element */
        m_mapFragment = getMapFragment();

        // Set path of disk cache
        String diskCacheRoot = m_activity.getFilesDir().getPath()
                + File.separator + ".isolated-here-maps";

        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(diskCacheRoot);
        if (!success) {
            // Setting the isolated disk cache was not successful, please check if the path is valid and
            // ensure that it does not match the default location
            // (getExternalStorageDirectory()/.here-maps).
        } else {
            if (m_mapFragment != null) {
                /* Initialize the AndroidXMapFragment, results will be given via the called back. */
                m_mapFragment.init(new OnEngineInitListener() {
                    @Override
                    public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {

                        if (error == Error.NONE) {
                            /* get the map object */
                            m_map = m_mapFragment.getMap();

                            /*
                             * Set the map center to the 4350 Still Creek Dr Burnaby BC (no animation).
                             */
                            m_map.setCenter(new GeoCoordinate(49.259149, -123.008555, 0.0),
                                    Map.Animation.NONE);

                            /* Set the zoom level to the average between min and max zoom level. */
                            m_map.setZoomLevel((m_map.getMaxZoomLevel() + m_map.getMinZoomLevel()) / 2);
                        } else {
                            new AlertDialog.Builder(m_activity).setMessage(
                                    "Error : " + error.name() + "\n\n" + error.getDetails())
                                    .setTitle(R.string.engine_init_error)
                                    .setNegativeButton(android.R.string.cancel,
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(
                                                        DialogInterface dialog,
                                                        int which) {
                                                    m_activity.finish();
                                                }
                                            }).create().show();
                        }
                    }
                });
            }
        }

    }


    private void initCreateRouteButton() {
        Button m_createRouteButton = (Button) m_activity.findViewById(R.id.button);

        m_createRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 * Clear map if previous results are still on map,otherwise proceed to creating
                 * route
                 */
                if (m_map != null && m_mapRoute != null) {
                    m_map.removeMapObject(m_mapRoute);
                    m_mapRoute = null;
                } else {
                    createRoute(RouteOptions.TransportMode.TRUCK);
                    //  createRoute(RouteOptions.TransportMode.CAR);
                }
            }
        });

    }

    // for store and show speed limits
    private List<String> speedLimits = new ArrayList<>();
    private boolean firstRouteCalculated = false;

    /* Creates a route*/
    private void createRoute(final RouteOptions.TransportMode transportMode) {
        CoreRouter coreRouter = new CoreRouter();
        RoutePlan routePlan = new RoutePlan();

        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(transportMode);
        routeOptions.setRouteType(RouteOptions.Type.FASTEST);
        routeOptions.setHighwaysAllowed(true);
        routeOptions.setTruckLength(16f);
        routeOptions.setTruckLimitedWeight(35f);
        routeOptions.setRouteCount(1);
        routePlan.setRouteOptions(routeOptions);

        RouteWaypoint startPoint = new RouteWaypoint(new GeoCoordinate(53.018382, 8.915880));
        RouteWaypoint destination = new RouteWaypoint(new GeoCoordinate(53.047492, 9.001711));

        routePlan.addWaypoint(startPoint);
        routePlan.addWaypoint(destination);

        coreRouter.calculateRoute(routePlan,
                new Router.Listener<List<RouteResult>, RoutingError>() {
                    @Override
                    public void onProgress(int i) {
                        /* The calculation progress can be retrieved in this callback. */
                    }

                    @Override
                    public void onCalculateRouteFinished(List<RouteResult> routeResults,
                                                         RoutingError routingError) {
                        /* Calculation is done. Let's handle the result */
                        if (routingError == RoutingError.NONE) {
                            if (routeResults.get(0).getRoute() != null) {
                                /* Create a MapRoute so that it can be placed on the map */
                                m_mapRoute = new MapRoute(routeResults.get(0).getRoute());

                                /* Show the maneuver number on top of the route */
                                m_mapRoute.setManeuverNumberVisible(true);

                                /* Add the MapRoute to the map */
                                m_map.addMapObject(m_mapRoute);

                                GeoBoundingBox gbb = routeResults.get(0).getRoute()
                                        .getBoundingBox();
                                m_map.zoomTo(gbb, Map.Animation.NONE,
                                        Map.MOVE_PRESERVE_ORIENTATION);
                                NavigationManager.getInstance().startNavigation(routeResults.get(0).getRoute());

                                for (RouteElement routeElement : routeResults.get(0).getRoute().getRouteElements().getElements()) {
                                    String logText = transportMode + " LinkId " +
                                            routeElement.getRoadElement().getPermanentLinkId() +
                                            " Speed Limit " +
                                            meterPerSecToKmPerHour(routeElement.getRoadElement().getSpeedLimit()) +
                                            "kph";

                                    speedLimits.add(logText);
                                    Log.d("SPEED_LIMIT", logText);

                                    List<TruckRestriction> truckRestrictionList = TruckRestrictionsChecker.getTruckRestrictions(routeElement.getRoadElement());
                                    for (TruckRestriction tr : truckRestrictionList) {
                                        if (tr.getType() == TruckRestriction.Type.SPEED_LIMIT) {
                                            Log.d("TRUCK_SPEED_LIMIT", tr.getValue() + "");
                                        }

                                        Log.d("TRUCK_RESTRICTION", tr.getType() + " " + tr.getValue() + "");
                                    }
                                }


                                //show dialog with speed limits from 2 types of road
                                if (firstRouteCalculated) {
                                    StringBuilder sb = new StringBuilder();
                                    for (String limit : speedLimits) {
                                        sb.append(limit).append("\n");
                                    }

                                    new AlertDialog.Builder(m_activity).setMessage(
                                            sb)
                                            .setTitle("Speed Limits")
                                            .setCancelable(false)
                                            .setNegativeButton(android.R.string.cancel,
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(
                                                                DialogInterface dialog,
                                                                int which) {
                                                            speedLimits.clear();
                                                            firstRouteCalculated = false;
                                                            dialog.dismiss();
                                                        }
                                                    }).create().show();
                                }
                                firstRouteCalculated = true;

                            } else {
                                Toast.makeText(m_activity,
                                        "Error:route results returned is not valid",
                                        Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(m_activity,
                                    "Error:route calculation returned error code: " + routingError,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private int meterPerSecToKmPerHour(double speed) {
        return (int) (speed * 3.6);
    }
}
