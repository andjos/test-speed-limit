# Speed Limit Warning for Android
---

As you can also read in our [documentation](https://developer.here.com/documentation/android-premium/dev_guide/topics/app-run-simple.html),
you should insert the credentials that you generated into the `<application>` tag:
```xml
    <meta-data android:name="com.here.android.maps.appid" android:value="{SAMPLE_APP_ID}"/>
    <meta-data android:name="com.here.android.maps.apptoken" android:value="{SAMPLE_APP_CODE}"/>
    <meta-data android:name="com.here.android.maps.license.key" android:value="{SAMPLE_LICENSE}"/>
```

# You can find calculation route logic in the MapFragmentView

## ROUTE 53.018382, 8.915880 - 53.047492, 9.001711